import React from 'react';
import {useState} from 'react';
import {useHistory} from 'react-router-dom';

export default function Form (props) {
  let history = useHistory ();

  const [Email, setEmail] = useState ('');
  const [Password, setPassword] = useState ('');
  const [showstring, setshowstring] = useState ('');

  console.log ('Email:' + Email);
  console.log ('password:' + Password);

  const submithandler = () => {
    console.log ('Submit');
    const formdata = {
      Email: Email,
      Password: Password,
    };
    if (formdata.Email.length > 0 && formdata.Password.length > 0) {
      console.log ('User Data Entered');
      setshowstring ('Enter valid Email id and password');
      if (/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test (formdata.Email) && formdata.Password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/)) {
        console.log ('Submitted Successfully');
        history.push ('/About');
      }
    } else {
      setshowstring ('Enter Email id and password both');
      console.log ('Enter Email id and password');
    }
  };

  return (
    <div className="row g-3 align-items-center">
    <h3 style={{color: "red"}}>{showstring}</h3>;
      <div className="col-auto">
        <label htmlFor="inputPassword6" className="col-form-label">
          {props.LabelMobile ? props.LabelMobile : "E-mail"}
        </label>
      </div>
      <div className="col-auto">
        <input
          type="E-mail"
          className="form-control"
          placeholder='Enter E-mail'
          aria-describedby="passwordHelpInline"
          onChange={e => setEmail (e.target.value)}
        />
      </div>
      <div className="col-auto">
        <label htmlFor="inputPassword6" className="col-form-label">
          Password
        </label>
      </div>

      <div className="col-auto">
        <input
          type="password"
          id="inputPassword6"
          className="form-control"
          placeholder='Enter Password'
          aria-describedby="passwordHelpInline"
          onChange={e => setPassword (e.target.value)}
        />
      </div>
      <button className="btn btn-primary" type="submit" onClick={submithandler}>
        Submit
      </button>;

      {/* <div className="col-auto">
      <span id="passwordHelpInline" className="form-text">
        Must be 8-20 characters long.
      </span>
    </div> */}
    </div>
  );
}
