import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes, Switch} from "react-router-dom";
import About from './Components/Props Concept/About';
import Contact from './Components/Props Concept/Contact';
import Goals from './Components/Props Concept/Goals';
import Pagenotfound from './Components/Route Concept/Pagenotfound';
import UsestateConcept from './Components/UsestateConcept';
import Form from './Components/Form Concept';
import Contactform from './Components/Contact Form';


function App() {
  return (
    <>
    <Switch>
      <Route path="/About" exact component={About}   />
      <Route path="/Contact" exact component={Contact}  />
      <Route path="/Goals" exact component={Goals}  />
      
      <Route path="/usestate" exact component={UsestateConcept}></Route>
      <Route path="/form" exact component={Form} />
      <Route path="/contactform" exact component={Contactform} />
      <Route  component={Pagenotfound}   />

    </Switch>
    </>
  );
}

export default App;
