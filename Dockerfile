# Selecting base Node image for react to build
FROM node  AS builder

# Setting working directory
WORKDIR /app

# Installing dependencies
COPY ./package.json .
RUN npm install

# Copying all the content of project dir to working dir.
COPY . .

# Building our application
RUN npm run build

#Using nginx to serve the static build dir
FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY --from=builder /app/build .

CMD ["nginx","-g","daemon off;"]

